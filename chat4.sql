-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-02-2012 a las 22:14:07
-- Versión del servidor: 5.5.20
-- Versión de PHP: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `chat4`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id_chat` int(11) NOT NULL AUTO_INCREMENT,
  `texto` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id_chat`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=286 ;

--
-- Volcado de datos para la tabla `chat`
--

INSERT INTO `chat` (`id_chat`, `texto`, `usuario`, `fecha`) VALUES
(130, 'ww', 'fsfafg', '2011-11-14'),
(138, 'sdsd', 'dffdf', '2011-11-20'),
(131, 'ssdd', 'fsfafg', '2011-11-14'),
(132, 'wwqw', 'terer', '2011-11-14'),
(133, 'qq', 'terer', '2011-11-14'),
(134, 'qq', 'fsfafg', '2011-11-14'),
(135, 'qq', 'fsfafg', '2011-11-14'),
(136, 'hola', 'fsfafg', '2011-11-14'),
(137, 'hola 2', 'fsfafg', '2011-11-14'),
(139, 'ee', 'fdasf', '2011-11-20'),
(82, 'hellas..', 'gfgg', '2011-11-13'),
(83, 'hellis', 'jyu', '2011-11-13'),
(84, 'hellus', 'jyu', '2011-11-13'),
(85, 'helles', 'jyu', '2011-11-13'),
(86, 'hullos', 'gfgg', '2011-11-13'),
(87, 'heellus', 'gfgg', '2011-11-13'),
(88, 'hiillos', 'gfgg', '2011-11-13'),
(89, 'hwlo', 'gfgg', '2011-11-13'),
(90, 'gool', 'gfgg', '2011-11-13'),
(91, 'gaal', 'gsdf', '2011-11-13'),
(92, 'geel', 'rete', '2011-11-13'),
(93, 'gaal', 'gfgff', '2011-11-13'),
(94, 'hi', 'gfgff', '2011-11-13'),
(99, 'hala', 'gfgff', '2011-11-13'),
(100, 'hele', 'rete', '2011-11-13'),
(101, 'hala', 'rete', '2011-11-13'),
(102, 'hile', 'rete', '2011-11-13'),
(103, 'hole', 'gfgff', '2011-11-13'),
(104, 'hule', 'gfgff', '2011-11-13'),
(105, 'hh', 'gsdf', '2011-11-13'),
(106, 'gf', 'gsdf', '2011-11-13'),
(107, 'gg', 'gsdf', '2011-11-13'),
(108, 'hh', 'gsdf', '2011-11-13'),
(109, '', 'bcbcb', '2011-11-13'),
(110, 'hi', 'bcbcb', '2011-11-13'),
(111, 'hjhj', 'bcbcb', '2011-11-13'),
(112, 'jj', 'bcbcb', '2011-11-13'),
(113, 'ññ', 'gsdf', '2011-11-13'),
(114, 'll', 'gsdf', '2011-11-13'),
(115, 'ññ', 'jkljkl', '2011-11-13'),
(116, 'nn', 'jkljkl', '2011-11-13'),
(117, 'mm', 'bcbcb', '2011-11-13'),
(118, 'oo', 'bcbcb', '2011-11-13'),
(119, 'pp', 'bcbcb', '2011-11-13'),
(120, 'l', 'bcbcb', '2011-11-13'),
(121, 'fg', 'bcbcb', '2011-11-13'),
(122, 'dsd', 'bcbcb', '2011-11-13'),
(123, 'ññññ', 'bcbcb', '2011-11-13'),
(124, 'er', 'fsdf', '2011-11-13'),
(125, 'hi', 'oioi', '2011-11-13'),
(126, 'ho', 'fsdf', '2011-11-13'),
(129, 'sdd', 'fsfafg', '2011-11-14'),
(95, 'he', 'gfgff', '2011-11-13'),
(96, 'ha', 'gfgff', '2011-11-13'),
(97, 'hala', 'rete', '2011-11-13'),
(98, 'hele', 'gsdf', '2011-11-13'),
(127, 'hi', 'fsdf', '2011-11-13'),
(128, 'hh', 'fsdf', '2011-11-13'),
(285, 'hola', 'oscalber', '2012-02-05'),
(7, 'ff', 'hh', '2011-08-26'),
(8, 'hh', 'hh', '2011-08-26'),
(9, 'ww', 'hh', '2011-08-26'),
(10, 'tt', 'hh', '2011-08-26'),
(11, 'ff', 'hh', '2011-08-26'),
(12, 'ff', 'hh', '2011-08-26'),
(13, 'gg', 'hh', '2011-08-26'),
(14, 'aaa', 'hh', '2011-08-26'),
(15, 'perame', 'hh', '2011-08-26'),
(16, 'sssssssssssssssssssssssss', 'hh', '2011-08-26'),
(17, 'hola a todos', 'oscalber', '2011-08-26'),
(18, 'hola', 'gg', '2011-08-26'),
(19, 'hello', 'gg', '2011-08-26'),
(20, 'perme', 'gg', '2011-08-26'),
(21, 'perame', 'gg', '2011-08-26'),
(22, 'hola hola', 'gg', '2011-08-26'),
(23, 'perame', 'gg', '2011-08-26'),
(24, 'ok', 'gg', '2011-08-26'),
(25, 'oye', 'gg', '2011-08-26'),
(26, 'esperame', 'gg', '2011-08-26'),
(27, 'ola', 'gg', '2011-08-26'),
(28, 'asd', 'gg', '2011-08-26'),
(29, 'aaaaaa', 'gg', '2011-08-26'),
(32, 'pera', 'gg', '2011-08-26'),
(31, '66', 'gg', '2011-08-26'),
(33, 'ik', 'gg', '2011-08-26'),
(34, 'oooh', 'gg', '2011-08-26'),
(35, 'hhh', 'gg', '2011-08-26'),
(36, 'hhh', 'gg', '2011-08-26'),
(37, 'j', 'gg', '2011-08-26'),
(38, 'ei', 'gg', '2011-08-26'),
(39, 'pera', 'gg', '2011-08-26'),
(40, 'oiga', 'gg', '2011-08-26'),
(41, 'ajam', 'll', '2011-08-26'),
(42, 'ok ok', 'll', '2011-08-26'),
(43, 'hoooola', 'll', '2011-08-26'),
(44, 'jjj', 'll', '2011-08-26'),
(45, 'oiga', 'll', '2011-08-26'),
(46, 'hola', 'll', '2011-08-26'),
(47, 'perame', 'll', '2011-08-26'),
(48, 'hola', 'll', '2011-08-26'),
(49, 'hoooooola', 'll', '2011-08-26'),
(50, 'como estas?', 'll', '2011-08-26'),
(51, 'holas', 'll', '2011-08-26'),
(52, 'hh', 'll', '2011-08-26'),
(53, 'aa', 'll', '2011-08-26'),
(54, 'ola', 'll', '2011-08-26'),
(55, 'hola', 'll', '2011-08-26'),
(56, 'D', 'll', '2011-08-26'),
(57, ':d', 'll', '2011-08-26'),
(58, 'OTRO', 'll', '2011-08-26'),
(59, 'gggggg', 'll', '2011-08-26'),
(60, 'perame', 'll', '2011-08-26'),
(61, 'ok', 'll', '2011-08-26'),
(62, 'pera', 'll', '2011-08-26'),
(63, 'hola', 'll', '2011-08-26'),
(64, 'hola', 'll', '2011-08-26'),
(65, 'hola hola', 'll', '2011-08-26'),
(66, 'aaasshhh', 'll', '2011-08-26'),
(67, 'oooooooooooooh', 'll', '2011-08-26'),
(68, 'prueba', 'll', '2011-08-26'),
(69, 'mmm', 'll', '2011-08-26'),
(70, 'nnnn', 'll', '2011-08-26'),
(71, 'fdfdf', 'll', '2011-08-26'),
(72, 'helo\n', 'll', '2011-08-26'),
(73, 'aaaah', 'll', '2011-08-26'),
(74, 'oooh', 'll', '2011-08-26'),
(75, 'oigame', 'll', '2011-08-26'),
(76, 'perame', 'll', '2011-08-26'),
(77, 'ayayay', 'll', '2011-08-26'),
(78, 'ouou', 'll', '2011-08-26'),
(79, 'aaay', 'll', '2011-08-26'),
(80, 'ooo', 'll', '2011-08-26'),
(81, 'uuu', 'll', '2011-08-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(20) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `fecha_registro` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `password`, `correo`, `fecha_registro`) VALUES
(9, 'oscalber', 'MTIzNDU2', 'oscalber_22@hotmail.com', '2011-12-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_temp`
--

CREATE TABLE IF NOT EXISTS `usuarios_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `usuarios_temp`
--

INSERT INTO `usuarios_temp` (`id`, `usuario`, `fecha`) VALUES
(2, 'oscar', '2012-02-19'),
(10, 'pedro', '2012-02-19');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
