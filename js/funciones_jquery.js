var minimoTiempoChat=3000;//milisegundos= 3 sg
var tiempoChat=minimoTiempoChat;
var titulo_original;
var foco_ventana=true;
var actual=0;
var titulo_cargado=0;
$(document).ready(function(){
titulo_original=document.title;
cargar_chat();
ventanaModal();
$([window, document]).blur(function(){
		foco_ventana = false;
	}).focus(function(){
		foco_ventana = true;
		document.title = titulo_original;
		
	});
})

function cargar_chat()
{
url="datos.php";	
 $.ajax({
	 url:url,
	 data:{"val":"valida_session"},
	 async:true,
	 //contenttype el tipo de contenido Se usa cuando se mandan datos a los servidores a modo de encabezado.
	 contentType:"application/x-www-form-urlencoded",
	 dataType:"json",
	 error:function()
	 {
		alert("ocurrio un error en ajax, en la funcion cargar_chat");	 
	 },
	 //Permite hacer que el objeto ajax obedezca o desobedezca las reglas para objetos ajax que el usuario pone
	 global:true,
	 //	Permite que el objeto ajax se active solo si la pagina a cargar se ha modificado.
	 ifModified: false,
	 processData:true,
	 success:function(datas){
	if(datas[0]["respuesta"]==1)
	{
		$("#contenedor_chat").remove();
		if($('#contenedor_chat').length==0)
		{	
			$("body").append('<div id="contenedor_chat"><a href="javascript:void(0);" id="mostrar_caja"><span>Abrir Chat</span></a></div>');
			$("#contenedor_chat").append('<div id="caja_chat"><a href="javascript:void(0);" id="titulo_caja">Chat</a><a href="javascript:void(0)" onclick="javascript:salir();" title="Cerrar Sesion"><img src="img/exit.png" alt="exit"></a></div>');
			$("#caja_chat").append('<div id="mensaje"></div><br />');
			$("#caja_chat").append('<div id="textarea_insertar_mensaje"><textarea class="textarea" onkeydown="javascript:validar_tecla(event,this,1)"></textarea></div>');
			$(".textearea").focus();
			$("#modal").css({"display":"none"});
			$("#mostrar_caja").click(function(event){
						event.preventDefault();
						$("#caja_chat").slideToggle();
						$("#mensaje").scrollTop($("#mensaje")[0].scrollHeight)
												   });
			
			$("#caja_chat a").click(function(event){
								event.preventDefault();
								$("#caja_chat").slideUp();
											 });
		ver_mensajes();
		}else
		{
		 ver_mensajes();	
		}
	 }else
	 {
	$("body").append('<div id="contenedor_chat"><a href="javascript:void(0);" id="mostrar_caja"><span>Logueate</span></a></div>');
	$("#contenedor_chat").append('<div id="caja_chat"><a href="javascript:void(0);" id="titulo_caja">Logueo</a></div>');	
	
	$("#mostrar_caja").click(function(event){
				event.preventDefault();
				$("#modal").css({"display":"block"});
				$("#contenido_modal").slideDown(1000);
										   });
	
	$("#caja_chat a").click(function(event){
						event.preventDefault();
						$("#caja_chat").slideUp();
									 });	
			}		 
		 
		 },
     //Permite definir un tiempo de espera antes de ejecutar un objeto ajax
	 timeout:3000,
	 type:"POST"
	 
	   });
  return false;		
}

function validar_tecla(event,campo,lugar)
{

	if(event.keyCode == 13 && event.shiftKey == 0)  {
		if(lugar==1)
		{
		enviar_mensaje(campo);
		}
		else
		{	
		logueo(campo);	
		}
	}
	
}

function form_generica(dato1,dato2,dato3,dato4,dato5)
{
		
		if(dato5=="registro"){
		$(".img_loading:eq(2)").html("<img src='img/loading.gif' border='0' />");	
		}else if(dato5=="logueo_re"){
		$(".img_loading:eq(1)").html("<img src='img/loading.gif' border='0' />");	
		}else if(dato5=="logueo_temp"){
		$(".img_loading:eq(0)").html("<img src='img/loading.gif' border='0' />");		
		}
		
		url="datos.php";
	
		
		$.ajax({
					 
					 url: url,
					 data: {"val":dato5,"dato1":dato1,"dato2":dato2,"dato3":dato3,"dato4":dato4},
					async:true,
					cache: false,
					
					contentType: "application/x-www-form-urlencoded",
					dataType: "json",
					error: function(objeto, quepaso, otroobj){
						alert("ocurrio un error en ajax, en la funcion form_generica");
					},
					global: true,
					ifModified: false,
					processData:true,
					
					success: function(datas) {
							
						$(".img_loading").html("");	 
						if(datas[0]["respuesta"]==1)
						{
							$('.mensaje_resp:eq(2)').css({"display":"block"});
							$(".mensaje_resp:eq(2)").html("<font color='#0F0'>Usuario Registrado</font>");
							$('.mensaje_resp:eq(2)').fadeOut(10000);
							$("#usuario_reg").val("");
							$("#correo_reg").val("");
							$("#pass_reg").val("");
							$("#rpass_reg").val("");
							
							return false;
						}else
						{
							if(datas[0]["respuesta"]==2){
								$('.mensaje_resp:eq(2)').css({"display":"block"});
								$(".mensaje_resp:eq(2)").html("<font color='#Ba1616'>Usuario o Correo ya registrados</font>");
								$('.mensaje_resp:eq(2)').fadeOut(10000);
								$("#usuario_reg").focus();
								$("#usuario_reg").val("");
								$("#correo_reg").val("");
							}else{
								if(datas[0]["respuesta"]==3){
									$('.mensaje_resp:eq(2)').css({"display":"block"});
								$(".mensaje_resp:eq(2)").html("<font color='#Ba1616'>Correo Invalido</font>");	
								$('.mensaje_resp:eq(2)').fadeOut(10000);
								$("#correo_reg").focus();
								$("#correo_reg").val("");
								}else{
									if(datas[0]["respuesta"]==4){
										$('.mensaje_resp:eq(2)').css({"display":"block"});
										$(".mensaje_resp:eq(2)").html("<font color='#Ba1616'>El password debe ser minimo de 6 digitos</font>");	
										$('.mensaje_resp:eq(2)').fadeOut(10000);
										$("#pass_reg").focus();
										$("#pass_reg").val("");
										$("#rpass_reg").val("");
									}else{
										if(datas[0]["respuesta"]==5){
											$('.mensaje_resp:eq(2)').css({"display":"block"});
											$(".mensaje_resp:eq(2)").html("<font color='#Ba1616'>Los password no coinciden</font>");
											$('.mensaje_resp:eq(2)').fadeOut(10000);		
											$("#pass_reg").focus();
											$("#pass_reg").val("");
											$("#rpass_reg").val("");
										
										}else{
											if(datas[0]["respuesta"]==6){
											$('.mensaje_resp:eq(2)').css({"display":"block"});
											$(".mensaje_resp:eq(2)").html("<font color='#Ba1616'>Llene todos los campos</font>");
											$('.mensaje_resp:eq(2)').fadeOut(10000);			
											$("#usuario_reg").focus();
											}else{
												
												if(datas[0]["respuesta"]==7){
													$('.mensaje_resp:eq(1)').css({"display":"block"});
													$(".mensaje_resp:eq(1)").html("<font color='#Ba1616'>Datos Incorrectos</font>");
													$('.mensaje_resp:eq(1)').fadeOut(10000);			
													$("#usuario_re").focus();	
												}else{
													
													if(datas[0]["respuesta"]==8){
														$('.mensaje_resp:eq(1)').css({"display":"block"});
														$(".mensaje_resp:eq(1)").html("<font color='green'>Bienvenido</font>");
														$('.mensaje_resp:eq(1)').fadeOut(10000);			
														$("#contenido_modal").slideUp(1000);
														$("#modal").fadeOut(1000);
														cargar_chat()

													}else{
														if(datas[0]["respuesta"]==9){
															
															$('.mensaje_resp:eq(0)').css({"display":"block"});
															$(".mensaje_resp:eq(0)").html("<font color='green'>Bienvenido</font>");
															$('.mensaje_resp:eq(0)').fadeOut(10000);			
															$("#contenido_modal").slideUp(1000);
															$("#modal").fadeOut(1000);
															cargar_chat()															


														}else{
															

															}if(datas[0]["respuesta"]==10){
																
																$('.mensaje_resp:eq(0)').css({"display":"block"});
																$(".mensaje_resp:eq(0)").html("<font color='#ba1616'>Ya existe</font>");
																
																

															}else{
																if(datas[0]["respuesta"]==11){
																	
																	$('.mensaje_resp:eq(0)').css({"display":"block"});
																	$(".mensaje_resp:eq(0)").html("<font color='green'>Bienvenido</font>");
																	$('.mensaje_resp:eq(0)').fadeOut(10000);			
																	$("#contenido_modal").slideUp(1000);
																	$("#modal").fadeOut(1000);
																	cargar_chat()
																}else{
																	
																	if(datas[0]["respuesta"]==12){
																		$('.mensaje_resp:eq(0)').css({"display":"block"});
																		$(".mensaje_resp:eq(0)").html("<font color='#ba1616'>Ya existe</font>");
																
																	}else{
																	   
                                                                       if(datas[0]["respuesta"]==13){
                                                                        $('.mensaje_resp:eq(0)').css({"display":"block"});
																		$(".mensaje_resp:eq(0)").html("<font color='#ba1616'>Ingrese Un Usuario</font>");
																
                                                                                                                                               
                                                                       }                                                                       
                                                                       
                                                                                                                                                                                                                     
																																		   
																	}

																}
																
															}
														

												}	
											  }

											}
										}
									}
								}

							}
						}
								
							
						},
					timeout: 3000,
					type: "POST"
					});
					//jQuery("#mensaje").scrollTop(jQuery("#mensaje")[0].scrollHeight);
		return false;	
}



function ver_mensajes()
{

		
		url="datos.php";
	
		
		$.ajax({
					 
					 url: url,
					 data: {"val":"ver_mensajes"},
					async:true,
					cache: false,
					
					contentType: "application/x-www-form-urlencoded",
					dataType: "json",
					error: function(objeto, quepaso, otroobj){
						//alert("ocurrio un error en ajax, o en el archivo llamado");
					},
					global: true,
					ifModified: false,
					processData:true,
					
					success: function(datos) {
						
					
						$("#mensaje").html('');	
						$.each(datos, function(indice, val){
												
						$("#mensaje").append(datos[indice]["usuario"]+" dice: "+datos[indice]["fecha"]+"<br />");
						$("#mensaje").append(datos[indice]["mensaje"]+"<br /><br />");
						});
							
						$("#mensaje").scrollTop($("#mensaje")[0].scrollHeight);
						
						revisa_cantidad_mensajes();
						//setTimeout('ver_mensajes();',tiempoChat);
						},
					timeout: 3000,
					type: "POST"
					});
					
		return false;	
}

function revisa_cantidad_mensajes()
{
		$(".textarea").focus(
		function(){
		document.title=titulo_original;
		});
		
		url="datos.php";
	
		
		$.ajax({
					 
					 url: url,
					 data: {"val":"revisa_cantidad_mensajes"},
					async:true,
					cache: false,
					
					contentType: "application/x-www-form-urlencoded",
					dataType: "json",
					error: function(objeto, quepaso, otroobj){
						//alert("Error en ajax");
					},
					global: true,
					ifModified: false,
					processData:true,
					
					success: function(datas) {
						
								if (actual==0)
								{
								actual=datas[0]["actual"];
								
								}
								if(actual==datas[0]["actual"])
								{	
								actual=datas[0]["actual"];
								
								if (foco_ventana == true)
									{				
									document.title=titulo_original;
									}
								ver_mensajes();
		  
								}
								
								if(actual<datas[0]["actual"])
								{
									titulo_cargado=1;
									actual=datas[0]["actual"];
									if (foco_ventana == false)
									{				
									document.title="Nuevo Mensaje";	
									ver_mensajes()
									}
								}
									

						
						},
					timeout: 3000,
					type: "POST"
					});
		//setTimeout('ver_mensajes();',chatHeartbeatTime);
		
					//jQuery("#mensaje").scrollTop(jQuery("#mensaje")[0].scrollHeight);
		return false;	
}


function enviar_mensaje(textarea)
{

		$(textarea).focus();
		var texto=$(textarea).val();		
		
		url="datos.php";
		$.ajax({
					 
					 url: url,
					 data: {"mensaje":texto,"val":"insertar_mensaje"},
					async:true,
					cache: false,
					
					contentType: "application/x-www-form-urlencoded",
					dataType: "json",
					error: function(objeto, quepaso, otroobj){
					alert("ocurrio un error en ajax, o en el archivo llamado");
					},
					global: true,
					ifModified: false,
					processData:true,
					
					success: function(datas) {
						
						if(datas[0]["respuesta"]==1)
						{
						$("#textarea_insertar_mensaje .textarea").val('');
						ver_mensajes();			
						}
							
						},
					timeout: 3000,
					type: "POST"
					});
					
		return false;	
}

function salir(){
        $("#modal").css({"display":"none"});
      url="datos.php";
    	$.ajax({
					 
					 url: url,
					 data: {"val":"salir"},
					async:true,
					cache: false,
					
					contentType: "application/x-www-form-urlencoded",
					dataType: "json",
					error: function(objeto, quepaso, otroobj){
						//alert("ocurrio un error en ajax, o en el archivo llamado");
					},
					global: true,
					ifModified: false,
					processData:true,
					
					success: function(datos) {
						
                        if(datos[0]["respuesta"]==1){
                        window.location.reload();   
                        }
						
						},
					timeout: 3000,
					type: "POST"
					});
    
}


function ventanaModal()
{
	$("body").append("<div id='modal'><div id='contenido_modal'> <a id='close_modal'>&#215;</a><fieldset><legend>Usuario Temporal</legend><label>Usuario:</label><input type='text' class='input' id='usuario_an'/><br /><input type='button' value='Entrar' name='btn_temp' id='btn' onclick='javascript:form_generica($(\"#usuario_an\").val(),\"\",\"\",\"\",\"logueo_temp\")' ><div class='img_loading'></div><div class='mensaje_resp'></div></fieldset> <fieldset><legend>Usuarios Registrados</legend><label>Usuario:</label><input type='text' class='input' id='usuario_re' /><label>Contrase&ntilde;a</label><br /><input type='password' clas='input' id='pass_re' /><div class='img_loading'></div><br /><input type='button' id='btn' value='Loguearme' onclick='javascript:form_generica($(\"#usuario_re\").val(),$(\"#pass_re\").val(),\" \",\" \",\"logueo_re\")'/><div class='mensaje_resp'></div></fieldset> <fieldset><legend>Registro</legend><label>Usuario:</label><input type='text' class='input' id='usuario_reg'/><br /><label>Correo:</label><input type='text' class='input' id='correo_reg' /><br /><label>Contrase&ntilde;a:</label><input type='password' class='input' id='pass_reg' /><br /><label>R-Contrase&ntilde;a</label><input type='password' class='input' id='rpass_reg' /><br /><input type='button' id='btn' value='Registrar' onclick='javascript:form_generica($(\"#usuario_reg\").val(),$(\"#correo_reg\").val(),$(\"#pass_reg\").val(),$(\"#rpass_reg\").val(),\"registro\")' /><div class='img_loading'></div><div class='mensaje_resp'></div></fieldset> </div></div>");
	//$(\"#usuario_reg\").val()
	$("#close_modal").click(function(){
		
		$("#contenido_modal").slideUp(1000);
		$("#modal").fadeOut(1000);
		
	});
}